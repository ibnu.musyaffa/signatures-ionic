import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  ElementRef,
  AfterViewInit,
} from "@angular/core";
import { ModalController } from "@ionic/angular";
import { FormGroup } from "@angular/forms";
import SignaturePad from "signature_pad";
import { HttpClient } from "@angular/common/http";
import { BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  @ViewChild("sPad", { static: true }) signaturePadElement;
  signaturePad: any;

  form: FormGroup;
  modal: any;
  signaturePadOptions: Object = {};
  public _signature: any = null;
  innerWidth: number;
  scannedCode = null;

  constructor(
    public modalCtrl: ModalController,
    private elementRef: ElementRef,
    private http: HttpClient,
    private barcodeScanner: BarcodeScanner
  ) {}

  /**
   * Sets the styles and initiates the canvas
   *
   * @memberof SignaturePage
   */
  ngOnInit(): void {
    this.innerWidth = window.innerWidth - 32;

    this.resize();
  }

  changeColor() {
    const r = Math.round(Math.random() * 255);
    const g = Math.round(Math.random() * 255);
    const b = Math.round(Math.random() * 255);
    const color = "rgb(" + r + "," + g + "," + b + ")";
    this.signaturePad.penColor = color;
  }

  clear() {
    this.signaturePad.clear();
  }

  undo() {
    const data = this.signaturePad.toData();
    if (data) {
      data.pop(); // remove the last dot or line
      this.signaturePad.fromData(data);
    }
  }

  // FORCE to recreate signature pad
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.resize();
  }

  resize() {
    const pagePadding = 32;
    const pageWidth = window.innerWidth;
    const pageheight = window.innerWidth / 2;
    this.innerWidth = pageWidth - pagePadding;

    const canvas: any = this.elementRef.nativeElement.querySelector("canvas");
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    const ratio = Math.max(window.devicePixelRatio || 1, 1);

    // const canvas: any = this.signaturePad._canvas;
    canvas.width = innerWidth - pagePadding;
    canvas.height = pageheight;
    if (this.signaturePad) {
      this.signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }
  }

  public ngAfterViewInit(): void {
    this.signaturePad = new SignaturePad(
      this.signaturePadElement.nativeElement
    );

    this.signaturePad.clear();
  }

  /**
   * Closes the modal and passes the signature to the page where it is being requested
   *
   * @memberof SignaturePage
   */
  save(): void {
    const img = this.signaturePad.toDataURL("image/png");
    let formData: any = new FormData();
    formData.append("image", img);

    this.http.post("http://127.0.0.1:8000/api/signature", formData).subscribe(
      (response) => {
        this.signaturePad.clear();
      },
      (error) => console.log(error)
    );
  }

  /**
   * returns true there is no signature, false if there is a signature
   *
   * @returns
   * @memberof SignaturePage
   */
  isCanvasBlank(): boolean {
    if (this.signaturePad) {
      return this.signaturePad.isEmpty() ? true : false;
    }
  }

  scanCode() {
    this.barcodeScanner
      .scan()
      .then((data) => {
        this.scannedCode = data.text;
      })
      .catch((err) => {});
  }
}
